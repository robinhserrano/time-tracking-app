import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';
import 'package:time_tracking_app/screens/working_time_screen.dart';

PreferredSizeWidget customAppBar(
    {required String title,
    required String projectNumber,
    required String category,
    required String member,
    required int appBarHexColor,
    required BuildContext context}){
    return AppBar(
        backgroundColor: Color(appBarHexColor),
        elevation: 0,
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: (){
                Navigator.pushReplacement(
                    context, 
                    MaterialPageRoute(builder: (context)=> 
                        WorkingTimeScreen(
                            category: category, 
                            projNum: projectNumber,
                            member: member
                        )
                    )
                );
            }
        ),
        flexibleSpace: SafeArea(child: 
            Container(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,                
                    children: [
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                                SizedBox(width: 32),
                                Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Text(title, style: GoogleFonts.allertaStencil(color: Colors.white, fontSize: 22)),
                                        Text("Projekt: "+projectNumber, style: TextStyle(fontFamily: 'Mulish', color: Colors.white, fontSize: 12))
                                    ],
                                )
                            ]
                        )
                    ]
                )
            )
        )
    );
}

