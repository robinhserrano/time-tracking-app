import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/widgets/txt_heading.dart';

class CustomNavCard extends StatefulWidget {
    final String title;
    final int btnHexColor;
    final Widget navigation;
    final String timeType;

const CustomNavCard({
    Key? key,
    required this.title,
    required this.btnHexColor,
    required this.navigation,
    required this.timeType
    }) : super(key: key);

@override
_CustomNavCardState createState() => _CustomNavCardState();
}

class _CustomNavCardState extends State<CustomNavCard> {
    var _startEndTime;
    var isActivated;

    Future<void> isTimeActivated() async{
        final timeType = widget.timeType;
        final prefs = await SharedPreferences.getInstance();
        setState(() {
            isActivated = prefs.getBool("${timeType}Activated");
            print( widget.title+"SDFDSFSDFSDFSDF"+isActivated.toString());
        });
    }

    Future<void> getStartTime() async{
        final prefs = await SharedPreferences.getInstance();
        final timeType = widget.timeType;
        final startTime = prefs.getString("${timeType}StartTime");
        final endTime = prefs.getString("${timeType}EndTime");
        setState(() {
            _startEndTime = "$startTime Uhr - $endTime Uhr";
        });
    }

    @override
    void initState() {
        isTimeActivated();
        getStartTime();
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Container(
            padding: EdgeInsets.fromLTRB(0,16, 0, 24),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            txtHeading(text: widget.title),
                            (_startEndTime !=null && isActivated==true)
                            ? Chip(
                                label: Text(_startEndTime),
                                labelStyle: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 14,
                                    color: Color(0xFFFFFFFF)
                                    ),
                                backgroundColor: Color(widget.btnHexColor),
                                )
                            :Text(
                                "hinzufügen oder bearbeiten",
                                style: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 14,
                                    color: Color(0xFFB9B9B9)
                                )
                            )
                        ]
                    ),
                    Row(
                        children: [
                            MaterialButton(
                                child: Icon(Icons.add),
                                onPressed: () {
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> widget.navigation));
                                },
                                color: Color(widget.btnHexColor),
                                textColor: Colors.white,
                                padding: EdgeInsets.symmetric(vertical: 16),
                                shape: CircleBorder(),
                                elevation: 0,
                            )
                        ],
                    )
                ]
            )
        );
    }
}