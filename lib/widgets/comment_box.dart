import 'package:flutter/material.dart';

Widget commentBox(){
    return Container(
        color: Color(0xFFE0E0E0),
        padding: EdgeInsets.fromLTRB(16, 0, 8, 16),
        height: 120,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Container(
                    padding: EdgeInsets.fromLTRB(0, 16, 8, 0),
                    child: CircleAvatar(
                        radius: 32.0,
                        backgroundImage: AssetImage("assets/images/user_profile_pic.png"),
                        backgroundColor: Colors.transparent,
                    )
                ),
                Expanded(
                    child: tffComment,
                )
            ],
        )
    );
}

Widget tffComment = TextFormField(
        keyboardType: TextInputType.multiline,
        maxLines: null,
        decoration: InputDecoration(
            hintText: 'Bemerkung hinzufügen',
            hintStyle: TextStyle(
                fontFamily: 'Mulish', 
                fontSize: 16, 
                color: Color(0xFFB9B9B9)
            ),
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent, width: 3)
            ),
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent, width: 3)
            ),
        ),
    );