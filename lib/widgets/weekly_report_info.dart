import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

Widget weeklyReportInfo(
    {required String image,
    required String date,
    required String btnTitle,
    required Widget navigation,
    required BuildContext context,
    required String btnIcon}) {
    return InkWell(
        onTap: (){
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> navigation));
        },
        child: Row(
            children: [
                Container(
                    margin: EdgeInsets.only(bottom:40),
                    child: Column(
                        children: [
                            ClipRRect(
                                child: Image(
                                image: AssetImage(image),
                                width: 64,
                                height: 54,
                                ),
                            ),
                        ],             
                    )
                ),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left:16),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text(
                                    date,
                                    style: GoogleFonts.allertaStencil(fontSize: 20)
                                ),
                                SizedBox(height: 16),
                                Container(
                                    color: Colors.black,
                                    padding: EdgeInsets.all(12),
                                    child: Wrap(
                                        direction: Axis.horizontal,
                                        children: [
                                            Text(btnTitle,
                                                style: GoogleFonts.roboto(
                                                    color: Colors.white,
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w600 
                                                )
                                            ),
                                            SizedBox(width: 10),
                                            ClipRRect(
                                                child: Image(
                                                image: AssetImage(btnIcon),
                                                width: 20,
                                                height: 20,
                                                ),
                                            ),
                                        ],
                                    )
                                )
                            ],
                        ),
                    ),
                )
            ],
        ),
    );
}