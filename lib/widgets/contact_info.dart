import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

Widget contactInfo(
    {required String image,
    required String contactNum1,
    required String contactNum2,
    required String contactNum3,
    required String email,
    required String website}) {
    return Container(
        //color: Colors.green,
        child: Row(
            children: [
                Container(
                    margin: EdgeInsets.only(bottom: 72),
                    child: Column(
                        children: [
                            ClipRRect(
                                child: Image(
                                image: AssetImage(image),
                                width: 64,
                                height: 54,
                                ),
                            ),
                        ],             
                    )
                ),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left:16),
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                contactNum1,
                                style: GoogleFonts.allertaStencil(fontSize: 20)
                            ),
                            Text(
                                contactNum2,
                                style: GoogleFonts.allertaStencil(fontSize: 20)
                            ),
                            Text(
                                contactNum3,
                                style: GoogleFonts.allertaStencil(fontSize: 20)
                            ),
                            Text(
                                email,
                                style: GoogleFonts.allertaStencil(fontSize: 20, letterSpacing: -2)
                            ),
                            SizedBox(height: 16),
                            Text(website,
                                style: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 12, 
                                    decoration: TextDecoration.underline,
                                    color: Color(0xFF00A4EA)
                                    )
                                )
                            ],
                        ),
                    ),
                )
            ],
        ),
    );
}