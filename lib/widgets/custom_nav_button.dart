import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

Widget customNavButton(
    {required String btnTitle,
    required String btnIcon}) {
    return InkWell(
        onTap: (){},
        child: Container(  
            color: Colors.black,
            padding: EdgeInsets.all(12),
            child: Wrap(
                direction: Axis.horizontal,
                children: [
                    Text(btnTitle,
                        style: GoogleFonts.roboto(
                            color: Colors.white,
                            fontSize: 16, 
                            fontWeight: FontWeight.w600
                        )
                    ),
                    SizedBox(width: 10),
                    ClipRRect(
                        child: Image(
                        image: AssetImage(btnIcon),
                        width: 20,
                        height: 20,
                        ),
                    ),
                ],
            ),
        ),
    );
}