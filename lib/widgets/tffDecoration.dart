import 'package:flutter/material.dart';

InputDecoration tffDecoration(){
    return InputDecoration(
        hintText: "Wahlen Sie bitte Kategorie aus",
        hintStyle: TextStyle(
            fontFamily: 'Mulish',
            color: Color(0xFFB9B9B9),
            fontStyle: FontStyle.italic,
            fontSize: 14
        ),
        suffixIcon: IconButton(
            onPressed: null,
            icon: Image.asset(
                "assets/images/subtract.png", 
                height: 16
            )
        ),
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFE0E0E0), width: 3)
        ),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color(0xFF8465FF), width: 3)
        ),
    );
}