import 'package:flutter/material.dart';
import '/widgets/time_list_wheel.dart';

Widget configStartEndTime({
    required String timeType,
    required int hexColor
    }){
    return Container(
        child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
            Container(
                child: TimeListWheel(isStartTime: true, timeType: timeType),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 4.0,
                                color: Color(0xFF4CB7E6)
                            ),
                        ),
                    ),
                ),
            Container(
                child: TimeListWheel(isStartTime: false, timeType: timeType),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 4.0,
                                color: Color(0xFF4CB7E6)
                            ),
                        ),
                    ),
                ),
            ],
        ),
    );
}