import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

Widget txtHeading({required String text}){
    return Text(
        text, 
        style: GoogleFonts.allertaStencil(fontSize: 22),
    );
}