import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

Widget contactPerson(
    {required String image,
    required String name,
    required String email}) {
    return Container(
        child: Row(
            children: [
                Container(
                    child: CircleAvatar(
                    radius: 32.0,
                    backgroundImage: AssetImage(image),
                    backgroundColor: Colors.transparent,
                    )
                ),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left:16),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text(
                                    "\n"+name,
                                    style: GoogleFonts.allertaStencil(fontSize: 20)
                                ),
                                Text(
                                    email,
                                    style: TextStyle(
                                        fontFamily: 'Mulish',
                                        fontSize: 16,
                                        decoration: TextDecoration.underline,
                                        letterSpacing: -1
                                    ),
                                ),
                                SizedBox(height: 8),
                                Container(
                                    width: 150,
                                    height: 25,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage("assets/images/contact_person_number.png"),
                                            fit: BoxFit.fitHeight
                                        )
                                    ),
                                )
                            ],
                        ),
                    ),
                )
            ],
        ),
    );
}


