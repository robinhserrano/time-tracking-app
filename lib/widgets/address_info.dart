import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

Widget addressInfo(
    {required String image,
    required String locationName,
    required String street,
    required String city}) {
    return Container(
        child: Row(
            children: [
                Column(children: [
                    ClipRRect(
                        child: Image(
                            image: AssetImage(image),
                            width: 64,
                            height: 54
                        ),
                    ),
                    SizedBox(height: 16)
                    ],
                ),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left:16),
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(
                                locationName,
                                style: GoogleFonts.allertaStencil(fontSize: 20)
                            ),
                            Text(
                                street,
                                style: TextStyle(fontFamily: 'Mulish', fontSize: 16, color: Colors.black),
                            ),
                            Text(
                                city,
                                style: TextStyle(fontFamily: 'Mulish', fontSize: 16, color: Colors.black),
                                ),
                            ],
                        ),
                    ),
                )
            ],
        ),
    );
}