import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> activateTime(String timeType) async{
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool("${timeType}Activated", true);
}

class AbortActivateTime extends StatelessWidget {
    const AbortActivateTime({
        Key? key,
        required this.navigation,
        required this.timeType
        }) : super(key: key);
    
    final Widget navigation;
    final String timeType;

    @override
    Widget build(BuildContext context) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
                InkWell(
                    onTap: (){
                        print("ACTIVATE");
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> navigation));
                    },
                    child: Container(  
                        color: Colors.white,
                        padding: EdgeInsets.all(12),
                        child: Wrap(
                            direction: Axis.horizontal,
                            children: [
                                Text("Abbrechen",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black,
                                        fontSize: 16, 
                                        fontWeight: FontWeight.w600
                                    )
                                ),
                            ],
                        ),
                    ),
                ),
                SizedBox(width: 16),
                InkWell(
                    onTap: (){
                        activateTime(timeType);
                        print("ACTIVATE");
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> navigation));
                    },
                    child: Container(  
                        color: Colors.black,
                        padding: EdgeInsets.all(12),
                        child: Wrap(
                            direction: Axis.horizontal,
                            children: [
                                Text("Speichern",
                                    style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontSize: 16, 
                                        fontWeight: FontWeight.w600
                                    )
                                ),
                                SizedBox(width: 10),
                                ClipRRect(
                                    child: Image(
                                    image: AssetImage("assets/images/send_klein.png"),
                                    width: 20,
                                    height: 20,
                                    ),
                                ),
                            ],
                        ),
                    ),
                )
            ],
        );
    }
}


