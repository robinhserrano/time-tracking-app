
import 'package:flutter/material.dart';
import 'package:clickable_list_wheel_view/clickable_list_wheel_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';


class TimeListWheel extends StatefulWidget {
    const TimeListWheel({Key? key, required bool isStartTime, required String timeType})
    : _isStartTime = isStartTime, _timeType = timeType;

    final bool _isStartTime;
    final String _timeType;
    @override
    _TimeListWheel createState() => _TimeListWheel();
}

class _TimeListWheel extends State<TimeListWheel> {
    final _scrollController = FixedExtentScrollController();
    int currentHour = DateTime.now().hour;
    List<String> hrsFifteenMinInterval = [];
    List<String> minutes = ["00","15","30","45"];
    final double _itemHeight = 50;
    late final int _itemCount;
    int currentIndex = 0;
    final assetImage = Image.asset('assets/images/play_icon.png');

    @override
    void initState() {
        currentIndex = 0;
        getHourWithFifteenMinuteInterval();
        _itemCount = hrsFifteenMinInterval.length;
        super.initState();
        
    }

    getHourWithFifteenMinuteInterval(){
        for (var hr = currentHour; hr < 24; hr++) {
            for (var i = 0; i < 4; i++) {
                setState(() {
                    hrsFifteenMinInterval.add(
                        hr % 12 == 0 
                        ? "12 : " + minutes[i]
                        : (hr%12).toString()+" : "+minutes[i]
                    );
                });
            }
        }
    }

    Future<void> setStartTime(String startTime) async{
        final prefs = await SharedPreferences.getInstance();
        final timeType = widget._timeType;
        await prefs.setString("${timeType}StartTime", startTime);
        print("START TIME -"+widget._timeType+" : "+ startTime);
    }

    Future<void> setEndTime(String endTime) async{
        final prefs = await SharedPreferences.getInstance();
        final timeType = widget._timeType;
        await prefs.setString("${timeType}EndTime", endTime);
        print("END TIME -"+widget._timeType+" : "+ endTime);
    }

    @override
    Widget build(BuildContext context) {
        return SafeArea(
            child: Container(
                height: 170,
                width: 170,
                child: ClickableListWheelScrollView(
                    scrollController: _scrollController,
                    itemHeight: _itemHeight,
                    itemCount: _itemCount,
                    onItemTapCallback: (index) {
                        setState(() {
                            currentIndex = index;
                        });
                        setStartTime(hrsFifteenMinInterval[index]);
                        print("onItemTapCallback index: $index");
                    },
                    child: ListWheelScrollView.useDelegate(
                        controller: _scrollController,
                        itemExtent: _itemHeight,
                        physics: FixedExtentScrollPhysics(),
                        overAndUnderCenterOpacity: 0.5,
                        perspective: 0.002,
                        onSelectedItemChanged: (index) {
                            setState(() {
                                currentIndex = index;
                            });
                            widget._isStartTime 
                                ? setStartTime(hrsFifteenMinInterval[index])
                                : setEndTime(hrsFifteenMinInterval[index]);
                            print("onSelectedItemChanged index: $index");
                        },
                        childDelegate: ListWheelChildBuilderDelegate(
                            builder: (context, index) => _child(index),
                            childCount: _itemCount,
                        ),
                    ),
                ),
            )
        );
    }

    Widget _child(int index) {
        return SizedBox(
            height: _itemHeight,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        IconButton(
                            padding: EdgeInsets.only(bottom: 16),
                            onPressed: (){},
                            icon: Icon(Icons.play_arrow)
                        ),
                        SizedBox(width: 8),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Text(hrsFifteenMinInterval[index] + " Uhr.", 
                                    style: GoogleFonts.allertaStencil()
                                ),
                                currentIndex == index 
                                    ? Text(widget._isStartTime ? 'Beginn': 'Ende',
                                        style: TextStyle(
                                            fontFamily: 'Mulish',
                                            fontSize: 12,
                                            color: Color(0xFFB9B9B9)
                                            )
                                        ) 
                                    : Text("")
                            ],
                        )
                    ]
                ),
            );
        }
    }
