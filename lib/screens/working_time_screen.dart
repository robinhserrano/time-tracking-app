import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';
import 'package:time_tracking_app/screens/time_table_screen.dart';

import '/screens/add_work_time_screen.dart';
import '/screens/pause_work_time_screen.dart';
import '/screens/home_screen.dart';

class WorkingTimeScreen extends StatefulWidget {
    const WorkingTimeScreen({ 
        Key? key, 
        required this.category, 
        required this.projNum,
        required this.member
    }) : super(key: key);

    final String category;
    final String projNum;
    final String member;

    @override
    _WorkingTimeScreenState createState() => _WorkingTimeScreenState();
}

class _WorkingTimeScreenState extends State<WorkingTimeScreen> {

    Widget buildBody(){ 
        return DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Scaffold(
                appBar: AppBar(
                    elevation: 0,
                    flexibleSpace: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                            TabBar(
                                indicator: UnderlineTabIndicator(
                                    borderSide: BorderSide(width: 3.0, color: Color(0xFF8465FF)),
                                    insets: EdgeInsets.symmetric(horizontal:16.0)),
                                tabs: [
                                Tab(child: Text(
                                        "Arbeitszeit",
                                        style: GoogleFonts.allertaStencil(fontSize: 18, color: Colors.black)
                                        )
                                    ),
                                Tab(child: Text(
                                        "Pause",
                                        style: GoogleFonts.allertaStencil(fontSize: 18, color: Colors.black)
                                    )
                                ),
                            ],
                        )
                    ],
                ),
            ),

            body: TabBarView(
                children: <Widget>[
                    AddWorkTimeScreen(category: widget.category, projNum: widget.projNum, member: widget.member),
                    PauseWorkTimeScreen()
                    ],
                ),
            ),
        );
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                elevation: 0,
                flexibleSpace: Container(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,                
                        children: [
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                    InkWell(
                                        child: Image.asset('assets/images/cross_button.png', height: 16),
                                        onTap: (){
                                            Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => TimeTableScreen()));
                                        }
                                    ),
                                    Image.asset("assets/images/app_logo.png", height: 32)
                                ]
                            )
                        ]
                    )
                )
            ),
            body: buildBody()
        );
    }
}