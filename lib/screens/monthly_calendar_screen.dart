import 'package:flutter/material.dart';
import '/screens/home_screen.dart';

class MonthyCalendarScreen extends StatefulWidget {
    const MonthyCalendarScreen({ Key? key }) : super(key: key);

    @override
    _MonthyCalendarScreenState createState() => _MonthyCalendarScreenState();
}

class _MonthyCalendarScreenState extends State<MonthyCalendarScreen> {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(backgroundColor: Colors.purple,
            leading: InkWell(
                onTap: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> HomeScreen()));
                    },
                    child: Icon(Icons.arrow_back_ios),
                )
            ),
            body: Center(
                child: Container(
                    color: Colors.purple,
                    child: Text("Monthly Calendar Screen\nWork in Progress", style: TextStyle(color: Colors.white)),
                )
            )
        );
    }
}