import 'package:flutter/material.dart';

import '/screens/working_time_screen.dart';
import '/widgets/comment_box.dart';
import '/widgets/config_start_end_time.dart';
import '/widgets/custom_app_bar.dart';
import '/widgets/abort_activate_time.dart';
import '/widgets/tffDecoration.dart';
import '/widgets/txt_heading.dart';

class StandbyTimeScreen extends StatefulWidget {
    const StandbyTimeScreen({ 
        Key? key, 
        required this.category, 
        required this.projNum,
        required this.member
    }) : super(key: key);

    final String category;
    final String projNum;
    final String member;

    @override
    _StandbyTimeScreenState createState() => _StandbyTimeScreenState();
}

class _StandbyTimeScreenState extends State<StandbyTimeScreen> {
    @override
    Widget build(BuildContext context) {

        Widget tffCategory(){
            return  TextFormField(
                initialValue: widget.category,
                keyboardType: TextInputType.text,
                decoration: tffDecoration(),
                textInputAction: TextInputAction.next,
                validator: (value) {
                    return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid category.';
                }
            );
        }

        Widget buildBody(){
            return ListView(
            padding: EdgeInsets.all(16),
                children: [
                    txtHeading(text: "Kategorie"),
                    tffCategory(),
                    SizedBox(height: 16),
                    txtHeading(text: "Bereitschaftszeit"),
                    configStartEndTime(timeType: "standby", hexColor: 0xFF8465FF),
                    SizedBox(height: 32),
                    commentBox(),
                    SizedBox(height: 32),
                    AbortActivateTime(
                        navigation: WorkingTimeScreen(
                            category: widget.category, 
                            projNum: widget.projNum, 
                            member: widget.member), 
                        timeType: "standby"),
                    SizedBox(height: 64)
                ],
            );
        }
        
        return Scaffold(
            appBar: customAppBar(
                title: "Bereitschaftszeit", 
                category: widget.category,
                projectNumber: widget.projNum, 
                member: widget.member,
                appBarHexColor: 0xFF8465FF, 
                context: context),
            body: buildBody(),
        );
    }
}