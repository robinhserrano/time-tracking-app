import 'package:flutter/material.dart';
import 'package:time_tracking_app/screens/time_table_screen.dart';
import '/screens/account_screen.dart';
import '/screens/business_card_screen.dart';
import '/screens/working_time_screen.dart';
//import '/screens/calendar_screen.dart';

class HomeScreen extends StatefulWidget {
    @override
    _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
    GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
    int _selectedPage = 0;

    final List<Widget> _fragmentView = [
        AccountScreen(),
        BusinessCardScreen(),
        //WorkingTimeScreen(category: "", projNum: "", member: ""),
        //CalendarScreen()
        TimeTableScreen(),
        TimeTableScreen()
        //TimetableExample()
        //ExampleApp(child: TimetableExample())
    ];
    
    @override
    Widget build(BuildContext context) {
        return SafeArea(
            child: Scaffold(
                appBar: _selectedPage!=2
                    ? AppBar(
                        leading: InkWell(
                            child: Image.asset('assets/images/menu_icon.png'),
                            onTap: (){ 
                                setState(() {
                                _drawerKey.currentState!.openDrawer();
                                });
                            }),
                            elevation: 0) 
                    : null,
                key: _drawerKey,
                body: _fragmentView[_selectedPage],
                drawer: Drawer(
                    child: Container(
                        padding: EdgeInsets.all(12),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                Container(
                                    padding: EdgeInsets.symmetric(horizontal: 24),
                                    margin: EdgeInsets.symmetric(vertical : 24),
                                    child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                        InkWell(
                                            child: Image.asset('assets/images/cross_button.png', height: 16),
                                            onTap: (){
                                                _drawerKey.currentState!.openEndDrawer();
                                            },
                                        ),
                                        Image.asset("assets/images/app_logo.png", height: 32),
                                        ]
                                    )
                                ),
                                Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                            _drawerItem(
                                                icon: "assets/images/user_icon.png", 
                                                title: "Mein Konto",
                                                selectedPage: 0
                                            ),  
                                            _drawerItem(
                                                icon: "assets/images/visitenkarte.png", 
                                                title: "Visitenkarte",
                                                selectedPage: 1
                                            ),   
                                            _drawerItem(
                                                icon: "assets/images/time_tracking.png", 
                                                title: "Zeiterfassung",
                                                selectedPage: 2
                                            ),      
                                            _drawerItem(
                                                icon: "assets/images/union.png", 
                                                title: "Meine Einsätze",
                                                selectedPage: 3
                                            )
                                        ]
                                    )
                                )
                            ]
                        )
                    )
                )
            )
        );
    }

    _drawerItem(
        {required String icon,
        required String title,
        required int selectedPage}){
            
        return InkWell(
            onTap: (){
                setState(() {
                    _selectedPage = selectedPage;
                    _drawerKey.currentState!.openEndDrawer();
                });
            },
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 4),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                        Image.asset(
                            icon,
                            width: 36,
                            height: 36,
                            color: _selectedPage == selectedPage
                            ? Color(0xFF000000)
                            : Color(0xFFE0E0E0)
                        ),
                        Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Column(
                            children: [
                                    Text(title, 
                                        style: TextStyle(
                                            color: _selectedPage == selectedPage
                                                ? Color(0xFF191D26)
                                                : Color(0xFF000000),
                                            fontWeight: _selectedPage == selectedPage
                                                ? FontWeight.w600
                                                : FontWeight.w500
                                        )
                                    ),
                                    _selectedPage == selectedPage
                                    ? ConstrainedBox(constraints: 
                                        BoxConstraints(maxWidth: 100),
                                        child: Divider(color: Color(0xFF8465FF), 
                                        thickness: 4, 
                                        height: 24),
                                    ) : Text("")
                                ]
                            )
                        )
                    ]
                )
            )
        );
    }
}