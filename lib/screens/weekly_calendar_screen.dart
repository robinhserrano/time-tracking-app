import 'package:flutter/material.dart';
import '/screens/home_screen.dart';

class WeeklyCalendarScreen extends StatefulWidget {
    const WeeklyCalendarScreen({ Key? key }) : super(key: key);

    @override
    _WeeklyCalendarScreenState createState() => _WeeklyCalendarScreenState();
}

class _WeeklyCalendarScreenState extends State<WeeklyCalendarScreen> {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(backgroundColor: Colors.purple,
            leading: InkWell(
                onTap: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> HomeScreen()));
                    },
                    child: Icon(Icons.arrow_back_ios),
                )
            ),
            body: Center(
                child: Container(
                    color: Colors.purple,
                    child: Text("Weekly Calendar Screen\nWork in Progress", style: TextStyle(color: Colors.white)),
                )
            )
        );
    }
}