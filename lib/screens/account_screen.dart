import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';
import 'package:time_tracking_app/screens/weekly_calendar_screen.dart';

import '/widgets/contact_person.dart';
import '/widgets/custom_nav_button.dart';
import '/widgets/single_worker.dart';
import '/widgets/txt_heading.dart';
import '/widgets/weekly_report_info.dart';
import '/widgets/monthly_report_info.dart';
import 'monthly_calendar_screen.dart';

class AccountScreen extends StatefulWidget {
    @override
    _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
    Widget buildBody(){ 
        return ListView(
            padding: EdgeInsets.all(20),
            children: [
                Container(
                    padding: EdgeInsets.all(16),
                    color: Colors.white,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            txtHeading(text: "Mein Konto"),
                            singleWorker(
                                name: "Greg Nue", 
                                email: "max.mustermann@bkl.de",
                                profession: "Monteur",
                                image: "assets/images/user_profile_pic.png"
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 24),
                                child: txtHeading(text: "Ansprechpartner"),
                            ),
                            contactPerson(
                                name: "Ingo Flamingo", 
                                email: "ingo.flamingo@f-bootcamp.com",
                                image: "assets/images/contact_person_pic.png"
                            ),
                            Container(
                                margin: EdgeInsets.symmetric(vertical: 24),
                                child: txtHeading(text: "Wochenbericht"),
                            ),
                            weeklyReportInfo(
                                date:"12.03 - 19.03.2021",
                                btnTitle: "Wochenbericht zuschicken",
                                btnIcon: "assets/images/send_klein.png",
                                image: "assets/images/calendar.png",
                                navigation: WeeklyCalendarScreen(),
                                context: context
                            ),
                            Container(
                                margin: EdgeInsets.symmetric(vertical: 24),
                                child: txtHeading(text: "Monatsbericht")
                            ),
                            monthlyReportInfo(
                                date:"April 2020",
                                btnTitle: "Monatsbericht erstellen",
                                btnIcon: "assets/images/send_klein.png",
                                image: "assets/images/calendar.png",
                                navigation: MonthyCalendarScreen(),
                                context: context
                            ),
                        ]
                    )
                ),
                overviewCard(),
                currentBudgetCard(),
                sickDaysCard(),
                accountHoursCard()
            ]
        );
    }

    Widget overviewCard(){
        return Container(
            color: Colors.white,
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.only(top: 24),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        margin: EdgeInsets.only(bottom: 16),
                        child: txtHeading(text: "Übersicht 2021")
                    ),
                    overviewDetails(
                        description: "Jahresurlaub", 
                        amount: 25
                    ),
                    overviewDetails(
                        description: "Resturlaub EPOS", 
                        amount: 10
                    ),
                    overviewDetails(
                        description: "Beantragt", 
                        amount: 08
                    ),
                    overviewDetails(
                        description: "Übertrag Vorjahr", 
                        amount: 01
                    ),
                    Text("(gültig bis 31.03.2021)", style: TextStyle(fontFamily: 'Mulish')),
                    Container(
                        margin: EdgeInsets.only(top: 24),
                        child: customNavButton(
                            btnTitle: "Urlaub beantragen", 
                            btnIcon: "assets/images/plus_klein.png"
                        )
                    )
                ]
            )
        );
    }

    Widget currentBudgetCard(){
        return Container(
            color: Color(0xFFE0E0E0),
            padding: EdgeInsets.fromLTRB(16, 24, 0, 24),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Text("Aktuelles Budget", style: GoogleFonts.allertaStencil(fontSize: 16)),
                    MaterialButton(
                        child: Text("7", style: GoogleFonts.allertaStencil()),
                        onPressed: () {},
                        color: Color(0xFFFFB72B),
                        textColor: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 16),
                        shape: CircleBorder(),
                        elevation: 0,
                    )
                ]
            )
        );
    }

    Widget sickDaysCard(){
        return Container(
            color: Colors.white,
            padding: EdgeInsets.all(16),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        margin: EdgeInsets.symmetric(vertical: 24),
                        child: txtHeading(text: "Krankheitstage")
                    ),
                    SizedBox(height: 12),
                    overviewDetails(
                        description: "Insgesamt", 
                        amount: 03
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 24),
                        child: customNavButton(
                            btnTitle: "Krankheit einreichen", 
                            btnIcon: "assets/images/plus_klein.png"
                        )
                    )
                ]
            )
        );
    }

    Widget accountHoursCard(){
        return Container(
            color: Colors.white,
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.only(top: 24),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        //margin: EdgeInsets.symmetric(vertical: 24),
                        child: txtHeading(text: "AZ Konto")
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Text(
                                "Stunden",
                                style: TextStyle(fontFamily: 'Mulish', fontSize: 16, color: Colors.black),
                            ),
                            Text(
                                "100 / 250",
                                style: GoogleFonts.allertaStencil(fontSize: 16, color: Color(0xFF191D26)),
                            )
                        ]
                    )
                ]
            )
        );
    }

    Widget overviewDetails({
        required String description, 
        required int amount}){
        return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
                Text(
                    description,
                    style: TextStyle(fontFamily: 'Mulish', fontSize: 16, color: Colors.black),
                ),
                Text(
                    amount.toString(),
                    style: GoogleFonts.allertaStencil(fontSize: 16, color: Color(0xFF191D26)),
                )
            ]
        );
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: buildBody(),
        );
    }
}