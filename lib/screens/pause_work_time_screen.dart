import 'package:flutter/material.dart';
import 'package:time_tracking_app/screens/home_screen.dart';

import '/screens/working_time_screen.dart';
import '/widgets/comment_box.dart';
import '/widgets/config_start_end_time.dart';
import '/widgets/abort_activate_time.dart';
import '/widgets/tffDecoration.dart';
import '/widgets/txt_heading.dart';

class PauseWorkTimeScreen extends StatefulWidget {
    @override
    _PauseWorkTimeScreenState createState() => _PauseWorkTimeScreenState();
}

class _PauseWorkTimeScreenState extends State<PauseWorkTimeScreen> {
    final _tffCategoryController = TextEditingController();

    @override
    void initState() {
        super.initState();
    }
    
    @override
    Widget build(BuildContext context) {
        return ListView(
            padding: EdgeInsets.all(16),
            children: [
                txtHeading(text: "Kategorie"),
                tffCategory(),
                SizedBox(height: 16),
                txtHeading(text: "Pause"),
                configStartEndTime(timeType: "pause", hexColor: 0xFF4CB7E5),
                SizedBox(height: 32),
                commentBox(),
                SizedBox(height: 32),
                AbortActivateTime(navigation: HomeScreen(),
                    timeType: "pause"
                ),
                SizedBox(height: 64)
            ],
        );
    }

    Widget tffCategory(){
        return  TextFormField(
            controller: _tffCategoryController,
            keyboardType: TextInputType.text,
            decoration: tffDecoration(),
            textInputAction: TextInputAction.next,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid category.';
            }
        );
    }
}