import 'package:flutter/material.dart';

import '/screens/working_time_screen.dart';
import '/widgets/comment_box.dart';
import '/widgets/config_start_end_time.dart';
import '/widgets/custom_app_bar.dart';
import '/widgets/abort_activate_time.dart';
import '/widgets/tffDecoration.dart';
import '/widgets/txt_heading.dart';

class WaitingTimeScreen extends StatefulWidget {
    const WaitingTimeScreen({ Key? key, required this.category, required this.projNum, required this.member}) : super(key: key);
    final String category;
    final String projNum;
    final String member;
    
    @override
    _WaitingTimeScreenState createState() => _WaitingTimeScreenState();
}

class _WaitingTimeScreenState extends State<WaitingTimeScreen> {

    @override
    Widget build(BuildContext context) {

        Widget tffCategory(){
            return  TextFormField(
                initialValue: widget.category,
                keyboardType: TextInputType.text,
                decoration: tffDecoration(),
                textInputAction: TextInputAction.next,
                validator: (value) {
                    return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid category.';
                }
            );
        }

        Widget buildBody(){
            return ListView(
                padding: EdgeInsets.all(16),
                children: [
                    txtHeading(text: "Kategorie"),
                    tffCategory(),
                    SizedBox(height: 16),
                    txtHeading(text: "Wartezeit"),
                    configStartEndTime(timeType: "waiting", hexColor: 0xFFFFB72B),
                    SizedBox(height: 32),
                    commentBox(),
                    SizedBox(height: 32),
                    AbortActivateTime(
                        navigation: WorkingTimeScreen(
                            category: widget.category, 
                            projNum: widget.projNum, 
                            member: widget.member), 
                        timeType: "waiting"
                    ),
                    SizedBox(height: 64)
                ],
            );
        }
        
        return Scaffold(
            appBar: customAppBar(
                title: "Wartezeit", 
                category: widget.category,
                projectNumber: widget.projNum, 
                member: widget.member,
                appBarHexColor: 0xFFFFB72B, 
                context: context),
            body: buildBody()
        );
    }
}