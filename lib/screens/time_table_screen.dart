import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:timetable/timetable.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:time_machine/time_machine.dart';

import '/screens/week_list_screen.dart';
import '/screens/home_screen.dart';
import '/screens/working_time_screen.dart';

class TimeTableScreen extends StatefulWidget {
    @override
    _TimeTableScreenState createState() => _TimeTableScreenState();
}

class _TimeTableScreenState extends State<TimeTableScreen> {
    String dateToday = DateFormat("MM.dd.yyyy").format(DateTime.now());
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
    late TimetableController<BasicEvent> _controller;

    @override
    void initState() {
        super.initState();

    _controller = TimetableController(
        eventProvider: EventProvider.list([
        BasicEvent(
            id: 0,
            title: "0,5 Std",
            color: Colors.blue[200],
            start: LocalDate.today().at(LocalTime(7, 0, 0)),
            end: LocalDate.today().at(LocalTime(8, 30, 0)),
            ),
        BasicEvent(
            id: 1,
            title: '0,5 Std',
            color: Colors.purple[300],
            start: LocalDate.today().at(LocalTime(10, 0, 0)),
            end: LocalDate.today().at(LocalTime(10, 30, 0)),
            ),
        BasicEvent(
            id: 2,
            title: '0,5 Std',
            color: Colors.red[300],
            start: LocalDate.today().at(LocalTime(10, 30, 0)),
            end: LocalDate.today().at(LocalTime(11, 00, 0)),
            ),
        ]),

        initialTimeRange: InitialTimeRange.range(
            startTime: LocalTime(7, 0, 0),
            endTime: LocalTime(20, 0, 0)
            ),
            initialDate: LocalDate.today(),
            visibleRange: VisibleRange.days(1),
            firstDayOfWeek: DayOfWeek.monday,
        );
    }

    @override
    void dispose() {
        _controller.dispose();
        super.dispose();
    }

    Widget buildBody(){
        return Timetable<BasicEvent>(
            controller: _controller,
            theme: TimetableThemeData(
                minimumHourHeight: 100,
                maximumHourHeight: 110,
                primaryColor: Colors.black
                
            ),
            onEventBackgroundTap: (start, isAllDay) {
            _showSnackBar('Background tapped $start is all day event $isAllDay');
            },
            eventBuilder: (event) {
            return BasicEventWidget(
                event,
                onTap: () => _showSnackBar('Part-day event $event tapped'),
            );
            },
            allDayEventBuilder: (context, event, info) => BasicAllDayEventWidget(
            event,
            info: info,
            onTap: () => _showSnackBar('All-day event $event tapped'),
            ),
        );
    }

    PreferredSizeWidget appbar(){
        return AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> HomeScreen()));
                }
            ),
            flexibleSpace: SafeArea(child: 
                Container(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,                
                        children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                    Container(
                                        padding: EdgeInsets.only(left: 16, right: 16),
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Text("Donnerstag", 
                                                    style: GoogleFonts.allertaStencil(
                                                        color: Colors.black, 
                                                        fontSize: 22
                                                    )
                                                ),
                                                Text(dateToday, 
                                                    style: TextStyle(
                                                        fontFamily: 'Mulish', 
                                                        color: Colors.black, 
                                                        fontSize: 12
                                                    )
                                                )
                                            ],
                                        ),  
                                    ),
                                    Container(
                                        child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                ElevatedButton(
                                                    onPressed: () {
                                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> WeekListScreen()));
                                                    },
                                                    child: Image.asset("assets/images/calendar_icon.png", height: 16),
                                                    style: ElevatedButton.styleFrom(
                                                        shape: CircleBorder(),
                                                        primary: Colors.white, 
                                                        side: BorderSide(width: 2.0, color: Colors.black,)
                                                    ),
                                                ),
                                                ElevatedButton(  
                                                    onPressed: () {
                                                        Navigator.pushReplacement(
                                                            context,
                                                            MaterialPageRoute(builder: (context)=> 
                                                                WorkingTimeScreen(
                                                                    category: "",
                                                                    projNum: "",
                                                                    member: "",
                                                                )
                                                            )
                                                        );
                                                    },
                                                    child: Icon(Icons.add, color: Colors.black),
                                                    style: ElevatedButton.styleFrom(
                                                        shape: CircleBorder(),
                                                        primary: Color(0xFFE0E0E0)
                                                    ),
                                                )
                                            ],
                                        ),
                                    )
                                ]
                            )
                        ]
                    )
                )
            )
        );
    }


    @override
    Widget build(BuildContext context) {
        return Scaffold(
        key: _scaffoldKey,
        appBar: appbar(),
        body: buildBody(),
        floatingActionButton: FloatingActionButton( 
            backgroundColor: Color(0xFF8465FF),
            onPressed: (){},        
            child: Text("03", style: GoogleFonts.allertaStencil(fontSize: 32))  
            ),
        );
    }

    void _showSnackBar(String content) {
        _scaffoldKey.currentState!.showSnackBar(SnackBar(
            content: Text(content),
        ));
    }
}