import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:time_tracking_app/screens/time_table_screen.dart';

class WeekListScreen extends StatefulWidget {
    const WeekListScreen({ Key? key }) : super(key: key);

    @override
    _WeekListScreenState createState() => _WeekListScreenState();
}

class _WeekListScreenState extends State<WeekListScreen> {
    String dateToday = DateFormat("MM.dd.yyyy").format(DateTime.now());
    
    Widget buildBody(){
        return ListView(
            children: [
                customListTile(title: "Freitag", subtitle: dateToday),
                customListTile(title: "Samstag", subtitle: dateToday),
                customListTile(title: "Sonntag", subtitle: dateToday)
            ],
        );
    }
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: appbar(),
            body: buildBody(),
        );
    }

    PreferredSizeWidget appbar(){
        return AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            leading: IconButton(
                icon: Image.asset("assets/images/cross_button.png", height: 16),
                onPressed: (){
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> TimeTableScreen()));
                }
            ),
            flexibleSpace: SafeArea(child: 
                Container(
                    padding: EdgeInsets.only(left: 0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,                
                        children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                    Container(
                                        padding: EdgeInsets.only(left: 16),
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Text("03", 
                                                    style: GoogleFonts.allertaStencil(
                                                        color: Colors.black, 
                                                        fontSize: 22
                                                    )
                                                ),
                                                Text("Kalenderwoche", 
                                                    style: TextStyle(
                                                        fontFamily: 'Mulish', 
                                                        color: Colors.black, 
                                                        fontSize: 12
                                                    )
                                                )
                                            ],
                                        ),  
                                    ),
                                    Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            ElevatedButton(
                                                onPressed: () {},
                                                child: Image.asset("assets/images/arrow_left.png", height: 16),
                                                style: ElevatedButton.styleFrom(
                                                    shape: CircleBorder(),
                                                    primary: Colors.white, 
                                                    elevation: 0
                                                ),
                                            ),
                                            ElevatedButton(  
                                                onPressed: () {},
                                                child: Image.asset("assets/images/arrow_right.png", height: 16),
                                                style: ElevatedButton.styleFrom(
                                                    shape: CircleBorder(),
                                                    primary: Colors.white,
                                                    elevation: 0
                                                ),
                                            )
                                        ],
                                    )
                                ]
                            )
                        ]
                    )
                )
            )
        );
    }

    Widget customListTile({
        required title,
        required subtitle,
        }){
        return ListTile(
            leading: Image.asset("assets/images/alarm_icon.png", height: 24),
            title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(title, 
                        style: GoogleFonts.allertaStencil(
                            color: Colors.black, 
                            fontSize: 22
                        )
                    ),
                    Text(subtitle, 
                        style: TextStyle(
                            fontFamily: 'Mulish', 
                            color: Colors.black, 
                            fontSize: 12
                        )
                    )
                ],
            ), 
        );
    }
}