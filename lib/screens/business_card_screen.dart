import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:google_fonts/google_fonts.dart';

import '/widgets/address_info.dart';
import '/widgets/contact_info.dart';
import '/widgets/single_worker.dart';
import '/widgets/txt_heading.dart';

class BusinessCardScreen extends StatefulWidget {
    @override
    _BusinessCardScreenState createState() => _BusinessCardScreenState();
}

class _BusinessCardScreenState extends State<BusinessCardScreen> {

    Widget buildBody(){ 
        return DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Scaffold(
                appBar: AppBar(
                flexibleSpace: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                        TabBar(
                            indicator: UnderlineTabIndicator(
                                borderSide: BorderSide(width: 3.0, color: Color(0xFF8465FF)),
                                insets: EdgeInsets.symmetric(horizontal:16.0)),
                            tabs: [
                            Tab(child: Text(
                                    "Meine Visitenkarte",
                                    style: GoogleFonts.allertaStencil(fontSize: 18, color: Colors.black)
                                    )
                                ),
                            Tab(child: Text(
                                    "Vorgesetzte",
                                    style: GoogleFonts.allertaStencil(fontSize: 18, color: Colors.black)
                                    )
                                )
                            ]
                        )
                    ]
                )
            ),

            body: TabBarView(
                children: <Widget>[
                    buildBusinessCard(),
                    buildSupervisorBusinessCard(),
                    ]
                )
            )
        );
    }

    Widget buildBusinessCard(){ 
        return ListView(
            padding: EdgeInsets.all(20),
            children: [
                Container(
                    padding: EdgeInsets.all(16),
                    color: Colors.white,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            txtHeading(text: "Visitenkarte"),
                            singleWorker(
                                name: "Greg Nue", 
                                email: "greg.neu@f-bootcamp.com",
                                profession: "Monteur",
                                image: "assets/images/user_profile_pic.png"
                            ),
                            Container(
                                padding: EdgeInsets.all(24),
                                child: Image.asset("assets/images/qrcode.png"),
                            ),
                            txtHeading(text: "Adresse"),
                            SizedBox(height: 16),
                            addressInfo(
                                locationName: "Flutter Bootcamp", 
                                street: "6783 Ayala Ave", 
                                city: "1200 Metro Manila",
                                image: "assets/images/address_icon.png"
                            ),
                            SizedBox(height: 32),
                            txtHeading(text: "Kontakt"),
                            SizedBox(height: 16),
                            contactInfo(
                                contactNum1: "T: +49 1234 56 789 01",
                                contactNum2: "F: +49 1234 56 789 01-2",
                                contactNum3: "M: +49 1234 56",
                                email: "E: greg.neu@f-bootcamp.com",
                                website: "www.flutter-bootcamp.com",
                                image: "assets/images/contact_icon.png"
                            )
                        ]
                    )
                )
            ]
        );
    }

    Widget buildSupervisorBusinessCard(){ 
        return ListView(
            padding: EdgeInsets.all(20),
            children: [
                Container(
                    padding: EdgeInsets.all(16),
                    color: Colors.white,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            txtHeading(text: "Visitenkarte"),
                            singleWorker(
                                name: "Greg Nue", 
                                email: "andero.mustermann@f-bootcamp.com",
                                profession: "Abteilungsleiter",
                                image: "assets/images/user_profile_pic.png"
                            ),
                            Container(
                                padding: EdgeInsets.all(24),
                                child: Image.asset("assets/images/qrcode.png"),
                            ),
                            txtHeading(text: "Adresse"),
                            SizedBox(height: 16),
                            addressInfo(
                                locationName: "Flutter Bootcamp", 
                                street: "6783 Ayala Ave", 
                                city: "1200 Metro Manila",
                                image: "assets/images/address_icon.png"
                            ),
                            SizedBox(height: 32),
                            txtHeading(text: "Kontakt"),
                            SizedBox(height: 16),
                            contactInfo(
                                contactNum1: "T: +49 1234 56 789 01",
                                contactNum2: "F: +49 1234 56 789 01-2",
                                contactNum3: "M: +49 1234 56",
                                email: "E: andreo.mustermann@f-bo...",
                                website: "www.flutter-bootcamp.com",
                                image: "assets/images/contact_icon.png"
                            )
                        ]
                    )
                )
            ]
        );
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: buildBody(),
        );
    }
}

