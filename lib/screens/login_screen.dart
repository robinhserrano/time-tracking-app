import 'package:flutter/material.dart';

import '/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
    @override
    _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    @override
    Widget build(BuildContext context) {
        
        Widget loginView = Container(
            padding: EdgeInsets.all(32),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                    Spacer(flex: 1),
                    Image.asset("assets/images/app_logo.png", height: 72),
                    Spacer(flex: 1),
                    Text("Flutter FieldPass", style: TextStyle(fontWeight: FontWeight.w700)),
                    Spacer(flex: 1),
                    Container(
                        child: Column(
                            children: [
                                ListTile(
                                    onTap: (){
                                        Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomeScreen()));
                                    },
                                    minLeadingWidth: 0,
                                    leading: Image.asset("assets/images/microsoft_logo.png", height: 28),
                                    title: Text(
                                        "Sign in with Microsoft", 
                                        style: TextStyle(
                                            fontFamily: 'Mulish',
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600
                                        )
                                    ),
                                    trailing: Image.asset("assets/images/arrow_double_klein.png", height: 20),
                                ),   
                                Divider(
                                    color: Color(0xFFE0E0E0), 
                                    thickness: 4, 
                                    height: 8
                                )
                            ],
                        ),
                    ),      
                    Spacer(flex: 3),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                            Text(
                                "Impressum",
                                style: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                            SizedBox(width: 36),
                            Text(
                                "Datenschutz",
                                style: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                        ]
                    )
                ],
            ),
        );

        return Scaffold(
            body: loginView,
        );
    }
}