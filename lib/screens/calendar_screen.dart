import 'package:flutter/material.dart';

class CalendarScreen extends StatefulWidget {
    @override
    _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
    int currentHour = DateTime.now().hour;
    List<String> hoursWithFifteenMinInterval = [];
    List<String> minutes = ["00","15","30","45"];
    
    getHourWithFifteenMinuteInterval(){
        for (var hr = currentHour; hr < 24; hr++) {
            for (var i = 0; i < 4; i++) {
                setState(() {
                    hoursWithFifteenMinInterval.add(hr%12 == 0 
                        ? "12:" + minutes[i]
                        : (hr%12).toString()+":"+minutes[i]
                    );
                });
            }
        }
    }
  
    @override
    void initState() {
        getHourWithFifteenMinuteInterval();
        super.initState();
    }

    
    @override
    Widget build(BuildContext context) {
        
         //   }
        //}
        return Container(
            color: Colors.purple,
            child: Text(hoursWithFifteenMinInterval.toString(), style: TextStyle(fontSize: 10, color: Colors.white),),
        );
    }
}