import 'package:flutter/material.dart';
import 'package:time_tracking_app/screens/home_screen.dart';

import '/screens/break_time_screen.dart';
import '/screens/standby_time_screen.dart';
import '/screens/waiting_time_screen.dart';
import '/screens/working_time_screen.dart';
import '/widgets/comment_box.dart';
import '/widgets/config_start_end_time.dart';
import '/widgets/custom_nav_card.dart';
import '/widgets/abort_activate_time.dart';
import '/widgets/tffDecoration.dart';
import '/widgets/txt_heading.dart';

class AddWorkTimeScreen extends StatefulWidget {
    const AddWorkTimeScreen({ Key? key, required this.category, required this.projNum, required this.member}) : super(key: key);
    final String category;
    final String projNum;
    final String member;
    
    @override
    _AddWorkTimeScreenState createState() => _AddWorkTimeScreenState();
}

class _AddWorkTimeScreenState extends State<AddWorkTimeScreen> {
    var _txtCategory;
    var _txtProjNum;
    var _member;

    @override
    Widget build(BuildContext context) {
        return ListView(
            padding: EdgeInsets.all(16),
            children: [
                txtHeading(text: "Kategorie"),
                tffCategory(),
                SizedBox(height: 16),
                txtHeading(text: "Projektnummer"),
                tffProjectNumber(),
                addMember(),
                SizedBox(height: 16),
                txtHeading(text: "Arbeitszeit"),
                configStartEndTime(timeType: "work", hexColor: 0xFF4CB7E5),
                (_txtCategory !=null && _txtProjNum !=null && _member !=null) 
                || ((widget.category !="" && widget.projNum !="" && widget.member !=""))
                ? Column(
                    children: [
                        CustomNavCard(
                            title: "Pause", 
                            btnHexColor: 0xFF6788FF, 
                            navigation: BreakTimeScreen(category: _txtCategory, projNum: _txtProjNum, member: _member), 
                            timeType: "break"
                        ),
                        CustomNavCard(
                            title: "Wartezeit", 
                            btnHexColor: 0xFFFFB72B, 
                            navigation: WaitingTimeScreen(category: _txtCategory, projNum: _txtProjNum, member: _member), 
                            timeType: "waiting"
                        ),
                        CustomNavCard(
                            title: "Bereitschaftszeit", 
                            btnHexColor: 0xFF8465FF, 
                            navigation: StandbyTimeScreen(category: _txtCategory, projNum: _txtProjNum, member: _member), 
                            timeType: "standby"
                        )
                    ]
                ) : SizedBox(height: 32),
                commentBox(),
                SizedBox(height: 32),
                AbortActivateTime(
                    navigation: HomeScreen(),
                    timeType: "work"
                ),
                SizedBox(height: 64)
            ],
        );
    }

    Widget tffCategory(){
        return  TextFormField(
            initialValue: widget.category == "" ? null: widget.category,
            keyboardType: TextInputType.text,
            decoration: tffDecoration(),
            textInputAction: TextInputAction.next,
            onChanged: (val){
                setState(() {
                    _txtCategory = val;
                });
            },
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid category.';
            }
        );
    }

    Widget tffProjectNumber(){
        return TextFormField(
            initialValue: widget.projNum == "" ? null: widget.projNum,
            keyboardType: TextInputType.number,
            decoration: tffDecoration(),
            textInputAction: TextInputAction.next,
            onChanged: (val){
                setState(() {
                    _txtProjNum = val;
                });
            },
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid project number.';
            }
        );
    }

    Widget addMember(){
        return Container(
            padding: EdgeInsets.fromLTRB(0,16, 0, 24),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            txtHeading(text: "Mitarbeiter"),
                            (_member!=null || widget.member!="")
                            ?Chip(
                                label: Text("Greg Nue"),
                                labelStyle: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 14,
                                    color: Color(0xFFFFFFFF)
                                    ),
                                backgroundColor: Colors.black,
                                )
                            :Text(
                                "hinzufügen oder bearbeiten",
                                style: TextStyle(
                                    fontFamily: 'Mulish',
                                    fontSize: 14,
                                    color: Color(0xFFB9B9B9)
                                )
                            )
                        ]
                    ),
                    MaterialButton(
                        child: Icon(Icons.add),
                        onPressed: () {
                            setState(() {
                              _member = "Greg Nue";
                            });
                        },
                        color: Colors.black,
                        textColor: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 16),
                        shape: CircleBorder(),
                        elevation: 0,
                    )
                ]
            )
        );
    }
}