import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:time_tracking_app/screens/login_screen.dart';

// ignore: import_of_legacy_library_into_null_safe
import 'package:time_machine/time_machine.dart';

void main() async{
    WidgetsFlutterBinding.ensureInitialized();
    await TimeMachine.initialize({'rootBundle': rootBundle});
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    await prefs.setString("Category", "AAA");
    runApp(App());
}

class App extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: LoginScreen(),
            theme: ThemeData(
                appBarTheme: AppBarTheme(
                    color: Colors.white
                ),
            ),
        );
    }
}