# Time Tracking App

## Setup Guide

This section focuses on the Step-by-step instruction to clone the project locally and install the needed Flutter packages.

1. Open GitBash or Terminal in your computer.
2. Change the current working directory to the location where you want the cloned directory.
3. Type `git clone`, and then paste the URL you copied earlier. `git clone <project_url>`.
4. Navigate back to the project's root folder by using the `cd ..` command.
5. Type `flutter pub get` to install the required packages in `pubspec.yaml` to your device.
6. Type `flutter run --no-sound-null-safety` to start building or running the cloned flutter application. (Note: Dart and Flutter SDK must be already installed in your computer, otherwise follow the instructions in the provided [link](https://flutter.dev/docs/get-started/install/windows)).

# Installed Packages

The following are the packages installed in pubspec.yaml with their respective description: 

- **[url_launcher](https://pub.dev/packages/url_launcher) 6.0.9**- Flutter plugin for launching a URL. Supports web, phone, SMS, and email schemes.
- [**clickable_list_wheel_view](https://pub.dev/packages/clickable_list_wheel_view): ^0.0.4 -** Simple wrapper for ListWheelScrollView that allows children to respond on gesture (onTap) events.
- [**shared_preferences](https://pub.dev/packages/shared_preferences): ^2.0.7 -** Flutter plugin for reading and writing simple key-value pairs. Wraps NSUserDefaults on iOS and SharedPreferences on Android.
- [**timetable](https://pub.dev/packages/timetable): 0.2.9 -** Customizable, animated calendar widget including day & week views.
- **[google_fonts](https://pub.dev/packages/google_fonts): ^1.1.2 -** Customizable, animated calendar widget including day & week views.
- **[intl](https://pub.dev/packages/intl): ^0.17.0** - Contains code to deal with internationalized/localized messages, date and number formatting and parsing, bi-directional text, and other internationalization issues.

## Flutter Version

The flutter version can be seen from pubspec.yaml which was set to ">=2.12.0 <3.0.0". 

The most important note in the version used was the introduction of Flutter null safety which is a feature that was made accessible following the release of version 2 of the Dart programming language, which has a minimum version of 2.12.

## App Features

### Login Feature

- Save token to app state
- Save token to local storage

The project management app recognizes only 3 types of Job Position namely: Contractor, Subcontractor, and Assembly Team, each of them has their unique access to the app's features. 

[Copy of Test User Credentials](https://www.notion.so/69cc34fe6d2744908d44f34e4f19d8f6)

### Contractor

- Show all projects
- Add project
- Assign project to subcontractor

### Subcontractor

- Show assigned projects
- Show project tasks
- Add task & assign to assembly team

### Assembly Team

- Show projects with assigned tasks
- Show projects tasks
- Tag Task as ongoing

### List of Screens in the App

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%201.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%202.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%203.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%204.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%205.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%206.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%207.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%208.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%209.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2010.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2011.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2012.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2013.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2014.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2015.png)

![Untitled](Time%20Tracking%20App%2018fc430660b641d1920aa557c7cf35d1/Untitled%2016.png)

[]()